public Insumo(){

}

public Insumo(Integer idInsumo, String estatusIns, String marcaIns, String modeloIns, String presentacion, String descripcionIns){
    this.idInsumo = idInsumo;
    this.estatusIns = estatusIns;
    this.marcaIns = marcaIns;
    this.modeloIns = modeloIns;
    this.presentacion = presentacion;
    this.descripcionIns = descripcionIns;
}

public Integer getIdInsumo(){
    return idInsumo;
}

public void setIdInsumo(Integer idInsumo){
    this.idInsumo = idInsumo;
}

public String getEstatusIns(){
    return estatusIns;
}

public void setEstatusIns(String estatusIns){
    this.estatusIns = estatusIns;
}

public String getMarcaIns(){
    return marcaIns;
}

public void setMarcaIns(String marcaIns){
    this.marcaIns = marcaIns;
}

public String getModeloIns(){
    return modeloIns;
}

public void setModeloIns(String modeloIns){
    this.modeloIns = modeloIns;
}

public String getPresentacion(){
    return presentacion;
}

public void setPresentacion(String presentacion){
    this.presentacion = presentacion;
}

public String getDescripcionIns(){
    return descripcionIns;
}

public void setDescripcionIns(String descripcionIns){
    this.descripcionIns = descripcionIns;
}
